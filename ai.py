import random
from xxlimited import new

from graphene import Int
from scipy import rand
from gui import *
from time import time
from game import Game, Interface
import json


# *** you can change everything except the name of the class, the act function and the sensor_data ***

class Agent:
    # ^^^ DO NOT change the name of the class ***

    def __init__(self):
        self.predicted_actions = []
        self.halat=0

    # the act function takes a json string as input
    # and outputs an action string
    # ('U' is go up,   'L' is go left,   'R' is go right,   'D' is go down,  'C' is clean tile)
    def act(self, percept):
        # ^^^ DO NOT change the act function above ***

        sensor_data = json.loads(percept)
        # ^^^ DO NOT change the sensor_data above ***

        alg = self.BFS_SAMPLE_CODE
    
        if self.predicted_actions == []:
            t0=time()
            initial_state=Game(sensor_data['Coordinates'], sensor_data['sticky_cubes'])
            self.predicted_actions = alg(initial_state)
            print("run time:", time()-t0)

        action = self.predicted_actions.pop()

        return action
    def deep(self ,  game,node_number,acted_list ,result): 
               
        actions = ['1','2','3']  
        if(node_number == 25):
            return result    
        for i in game.stucked_pices:
            if(i[0] == node_number+1):
                actions=["0"]
        if(node_number == 25):
            return [ acted_list ]
        for i in game.stucked_pices:
            if(i[0] == node_number+1):
                return self.deep(game,node_number+1,i,result) 
        new_result = []
        new_result+= result
        for j in result:
            acted_list = j
            for i in actions:        
                acted_list =  acted_list[:node_number:]+i+acted_list[node_number+1::]
                new_result.append(acted_list)
        return self.deep(game,node_number+1,i,new_result)
        
        
    def do_acction(self,node_number,game,acted_list ):
        interface=Interface()
        temp_game = interface.copy_state(game)
        actions = {'0':"0" , '1':'+90' , '2':'180' , '3':'-90'}
        gui = Graphics()
        
        for i in range(len(acted_list)-1,node_number,-1):
            first_cube , second_cube , third_cube = game.snake[node_number] ,game.snake[node_number+1],game.snake[node_number+2]
            if((first_cube[:2:] == second_cube[:2:]
            and second_cube[:2:] == third_cube[:2:])
            or (first_cube[::2] == second_cube[::2] 
            and second_cube[::2]  == third_cube[::2] )
            or (first_cube[1::] == second_cube[1::]
            and second_cube[1::] == third_cube[1::])):
                continue
            
            temp_game = interface.evolve(temp_game,(i ,actions[acted_list[i]]))
            if(interface.goal_test(game)):
                gui.display(game)
                return True
        return False
    def DFS_code(self,game,node_number,acted_list ):
        org_acted = acted_list
        interface=Interface()
        gui = Graphics()
        actions_list = ["1","2","3"]
             
        for i in game.stucked_pices:
            if(i[0] == node_number+1):
                input(i)
                actions_list=["0"]
                return self.DFS_code(game,node_number-1,org_acted )
             
        for i in actions_list:
            acted_list = org_acted[:node_number:]+ i +org_acted[node_number+1::]
            actions = {'0':"0" , '1':'+90' , '2':'180' , '3':'-90'}
            temp_game = interface.copy_state(game)
            
            if(interface.goal_test(game)):
                    return acted_list
            temp_game = interface.evolve(temp_game,(node_number , actions[acted_list[node_number]]))
            old_act =acted_list
            print(acted_list)

            res = self.deep(temp_game,node_number+1,acted_list,[old_act])
            
            for k in res:
                acted_list  = k
                print(acted_list )
                self.halat += 1
                if(self.do_acction(node_number,temp_game,acted_list)):
                    print('hi')
                    return acted_list
        if(node_number != 0):
            return self.DFS_code(game,node_number-1,org_acted )

        

        

    def BFS_SAMPLE_CODE(self, root_game):
        interface=Interface()
        seen_Nodes = []
        q = []
        a = self.DFS_code(root_game,24,'0'*25 )
        print(a)
        gui = Graphics()
        gui.display(root_game)
        input(self.halat)
        # append the first state as (state, action_history)
        node_number = 0
        q.append([root_game,node_number,[]])
        while q:
            # pop first element from queue
            node = q.pop(0)
            node_number = node[1]
            # get the list of legal actions
            print(node)
            actions_list = ["0","+90" , "-90" , "180"]
            # randomizing the order of child generation
            for i in seen_Nodes:
                if i.snake == node[0].snake :
                    continue
            random.shuffle(actions_list)
            #gui.display(node[0])
            seen_Nodes.append(node[0])
            first_cube , second_cube , third_cube = node[0].snake[node_number] ,node[0].snake[node_number+1],node[0].snake[node_number+2]
            if((first_cube[:2:] == second_cube[:2:]
            and second_cube[:2:] == third_cube[:2:])
            or (first_cube[::2] == second_cube[::2] 
            and second_cube[::2]  == third_cube[::2] )
            or (first_cube[1::] == second_cube[1::]
            and second_cube[1::] == third_cube[1::])):
                actions_list=["0"]                
            for i in node[0].stucked_pices:
                if(i[0] == node_number+1):
                    actions_list=["0"]

            
            for action in actions_list:
                # copy the current state
                child_state = interface.copy_state(node[0])
                # take action and change the copied node
                child_state=interface.evolve(child_state, (node_number , action))
                # add children to queue
                q.append([child_state, node_number+1,[action] + node[2]])
                # return if goal test is true
                
                if interface.goal_test(child_state):
                    print([action])
                    gui.display(node[0])
                    input()
                    return [action] + node[1]
