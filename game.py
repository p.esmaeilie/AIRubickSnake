from copy import deepcopy
import json
from os import stat

from sqlalchemy import false


class Game:
    def __init__(self, snake, stucked_pices):
        self.snake = snake
        self.stucked_pices = stucked_pices
    
    def do_rotation(self , left, right ,up,down,point ,rotate):
        if(rotate == "+90"):
            if(point ==left):
                return down
            elif(point ==right):
                return up
            elif(point ==up):
                return left
            elif(point==down):
                return right
        elif(rotate == "-90"):
            if(point ==left):
                return up
            elif(point ==right):
                return down
            elif(point ==up):
                return right
            elif(point==down):
                return left
        elif(rotate == "180"):
            if(point ==left):
                return right
            elif(point ==right):
                return left
            elif(point ==up):
                return down
            elif(point==down):
                return up
    def take_action(self, first_cube,rotate):  
        for i in self.stucked_pices:
            if(first_cube+1 == i[0]):
                return self.take_action(first_cube+1,rotate)
                
        if(first_cube+3 == len(self.snake)):
            return
        x1,y1,z1, = self.snake[first_cube]
        x2,y2,z2, = self.snake[first_cube+1]
        demention = ""
        if(x1 == x2 and y2 ==y1):
            demention = "z"
        elif(x1 == x2 and z2 ==z1):
            demention = "y"
        elif(z1 == z2 and y2 ==y1):
            demention = "x"
        x3,y3,z3 = self.snake[first_cube+2]
        if (demention == 'x' and x3 != x2)or (demention == 'y' and y3 != y2) or (demention == 'z' and z3 != z2)  :
            return
        # 0 1 1  , 1 1 1 , 1 1 2 =>   1 2 1 , 1
         
        if( demention == 'x' ):
            left = [x2 , y2 ,z2+1]
            right = [x2,y2,z2-1]
            up = [x2,y2-1 ,z2]
            down = [x2,y2+1 ,z2]
            self.snake[first_cube+2] = self.do_rotation(left,right,up,down,self.snake[first_cube+2],rotate)
        if( demention == 'y' ):
            left = [x2 , y2 ,z2+1]
            right = [x2,y2,z2-1]
            up = [x2-1,y2 ,z2]
            down = [x2+1,y2 ,z2]
            self.snake[first_cube+2] = self.do_rotation(left,right,up,down,self.snake[first_cube+2],rotate)
        if( demention == 'z' ):
            left = [x2 , y2+1 ,z2]
            right = [x2,y2-1,z2]
            up = [x2-1,y2 ,z2]
            down = [x2+1,y2 ,z2]
            self.snake[first_cube+2] = self.do_rotation(left,right,up,down,self.snake[first_cube+2],rotate)
        for i in range(first_cube+3,len(self.snake)):
            temp_x , temp_y,temp_z = self.snake[i]
            x , y, z = self.snake[i-1] 
            self.snake[i] = [x+temp_x -x3 ,y+ temp_y -y3, z +temp_z - z3 ]
            x3 , y3 , z3 = temp_x, temp_y,temp_z
        



class Interface:
    def __init__(self):
        pass

    # an example for what this class is supposed to do
    # here, it will make sure the action that is being
    # requested is in a correct format. this func won't return anything
    # the actual simulator must only deal with the game logic and nothing more
    def evolve(self, state, action):
        _cop = self.copy_state(state)
        if(action[1] == '0'):
            return state
        _cop.take_action(*action)
        
        if(self.valid_states(_cop)):
            state = _cop
        return state
        

    def copy_state(self, state):
        _copy = Game(None,None)
        _copy.snake = deepcopy(state.snake)
        _copy.stucked_pices = state.stucked_pices
        return _copy

    def perceive(self, state):
        pres = '{"Coordinates":'+str(state.snake)+', "sticky_cubes":'+str(state.stucked_pices)+'}'
        return str(pres)
    
    def goal_test(self, state):
        x_high , y_high , z_high = self.higher_corner(state)
        x_low , y_low, z_low= self.lower_corner(state)
        if not (x_high - x_low == 2 and z_high - z_low == 2  and y_high - y_low == 2  ):
            return False
        for i in range(len(state.snake)):
            x , y,z = state.snake[i]
            if not( x <= x_high and x >= x_low and 
                y <= y_high and y >= y_low and
                z <= z_high and z >= z_low  ):
                return False
        return True

    def lower_corner(self , state):
        min_x , min_y , min_z = state.snake[0]
        for i in range(1,len(state.snake)):
            x,y,z = state.snake[i]
            if(x <= min_x and y <= min_y and z <= min_z):
                min_x = x
                min_z = z
                min_y = y
        return [min_x,min_y , min_z]
    def higher_corner(self,state):
        max_x , max_y , max_z = state.snake[0]
        for i in range(1,len(state.snake)):
            x,y,z = state.snake[i]
            if(x >= max_x and y >= max_y and z >= max_z):
                max_x = x
                max_z = z
                max_y = y
        return [max_x,max_y , max_z]
    
    def valid_states(self, state):
        for i in range(len(state.snake)):
            for j in range(i+1 , len(state.snake)):
                if i == j:
                    return False
        return True