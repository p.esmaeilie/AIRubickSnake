from matplotlib.pyplot import xlabel
import pygame
import random
from numpy import array
from math import cos, sin



def rotation_matrix(α, β, γ):
    """
    rotation matrix of α, β, γ radians around x, y, z axes (respectively)
    """
    sα, cα = sin(α), cos(α)
    sβ, cβ = sin(β), cos(β)
    sγ, cγ = sin(γ), cos(γ)
    return (
        (cβ*cγ, -cβ*sγ, sβ),
        (cα*sγ + sα*sβ*cγ, cα*cγ - sγ*sα*sβ, -cβ*sα),
        (sγ*sα - cα*sβ*cγ, cα*sγ*sβ + sα*cγ, cα*cβ)
    )
BLACK, RED , BLUE , WITHE , GREEN , PURPEL,ORANGE= (80, 80, 80), (128, 0, 0) , (0,128,0) , (0,0,0),(0,0,128) , (128,128,0),(128,0,128)

class Physical:
    def __init__(self, vertices, edges , Color):
        """
        a 3D object that can rotate around the three axes
        :param vertices: a tuple of points (each has 3 coordinates)
        :param edges: a tuple of pairs (each pair is a set containing 2 vertices' indexes)
        """
        self.__vertices = array(vertices)
        self.__edges = tuple(edges)
        self.color = Color
        self.__rotation = [0, 0, 0]  # radians around each axis

    def rotate(self, axis, θ):
        self.__rotation[axis] += θ

    @property
    def lines(self):
        location = self.__vertices.dot(rotation_matrix(*self.__rotation))  # an index->location mapping
        return ((location[v1], location[v2]) for v1, v2 in self.__edges)


import threading
class Paint:
    def __init__(self, shape, keys_handler):
        self.__shapes = shape
        self.__keys_handler = keys_handler
        self.__size = 1000, 1000
        self.__clock = pygame.time.Clock()
        self.__screen = pygame.display.set_mode(self.__size)
        self.__thread = threading.Thread(target=self.__mainloop)
        self.__thread.start()
        
    
    def stop(self):
        pygame.quit()

    def __fit(self, vec):
        """
        ignore the z-element (creating a very cheap projection), and scale x, y to the coordinates of the screen
        """
        # notice that len(self.__size) is 2, hence zip(vec, self.__size) ignores the vector's last coordinate
        return [round(70 * coordinate + frame / 2) for coordinate, frame in zip(vec, self.__size)]

    def __handle_events(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                exit()
        self.__keys_handler(pygame.key.get_pressed())

    def __draw_shape(self, thickness=4):
        for shape in self.__shapes:
            for start, end in shape.lines:
                pygame.draw.line(self.__screen, shape.color, self.__fit(start), self.__fit(end), thickness)

    def __mainloop(self):
        while True:
            self.__handle_events()
            self.__screen.fill(BLACK)
            self.__draw_shape()
            pygame.display.flip()
            self.__clock.tick(40)
X, Y, Z = 0, 1, 2

class Graphics:
    def Vertic(self,  center):
        x = y = z =[0.125,-0.125]
        v= ()
        for i in x:
            for j in y:
                for k in z:
                    v = (*v,(center[0]/4+i , center[1]/4+j,center[2]/4+k))
        return v
    def __init__(self):
        self.p = None
    
    def check_func(self,index,sticky_pices):
        for i in sticky_pices:
            if(index == i[1]):
                return False
        return True
    def display(self, env):
        ls =[RED,BLUE,WITHE,GREEN,PURPEL,ORANGE]
        random.shuffle(ls)
        from pygame import K_q, K_w, K_a, K_s, K_z, K_x
        counter_clockwise = 0.05  # radians
        clockwise = -counter_clockwise
        cubes =[]
        last_color_used = ls.pop(0)
        for i in range(len(env.snake)):
            random.shuffle(ls)
            cubes.append(Physical( 
            vertices=self.Vertic(env.snake[i]),
            edges=({0, 1}, {0, 2}, {2, 3}, {1, 3},
                {4, 5}, {4, 6}, {6, 7}, {5, 7},
                {0, 4}, {1, 5}, {2, 6}, {3, 7}),
            Color=last_color_used
        ))
            if(self.check_func(i,env.stucked_pices)):
                ls.append(last_color_used)
                last_color_used = ls.pop(0)
                
        params = {
            K_q: (X, clockwise),
            K_w: (X, counter_clockwise),
            K_a: (Y, clockwise),
            K_s: (Y, counter_clockwise),
            K_z: (Z, clockwise),
            K_x: (Z, counter_clockwise),
        }

        def keys_handler(keys):
            for key in params:
                if keys[key]:
                    for i in cubes:
                        i.rotate(*params[key])
                    

        if(self.p is not None):
            self.p.stop()
        pygame.init()
        pygame.display.set_caption('Control -   q,w : X    a,s : Y    z,x : Z')
        
        self.p = Paint(cubes, keys_handler)
