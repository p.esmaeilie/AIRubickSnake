from ai import *
from game import *
from gui import *


sample_input_json={
    "sticky_cubes": [[5,6],[12,13],[14,15],[15,16],[18,19]],
    "‫‪Coordinates‬‬": [
        [-5,2,-6],
        [-5,2,-5],
        [-5,2,-4],
        [-4,2,-4],
        [-3,2,-4],
        [-3,2,-3],
        [-3,2,-2],
        [-2,2,-2],
        [-1,2,-2],
        [-1,2,-1],
        [0,2,-1],
        [0,2,0],
        [0,1,0],
        [0,0,0],
        [1,0,0],
        [2,0,0],
        [2,0,1],
        [3,0,1],
        [3,0,2],
        [3,0,3],
        [4,0,3],
        [4,0,4],
        [4,0,5],
        [5,0,5],
        [5,0,6],
        [6,0,6],
        [7,0,6]
    ]
}


if __name__ == "__main__":
    game = Game(sample_input_json['‫‪Coordinates‬‬'], sample_input_json['sticky_cubes'])
    interface=Interface()
    agent = Agent()
    gui = Graphics()

    action_count=0
    print("initial snake")
    while not (interface.goal_test(game)):
        
        action = agent.act(interface.perceive(game))
        
        print("attempting", action)
        interface.evolve(game, action)
        print("\n")
        action_count+=1

    print(
        "\n\nпобеда!!!",
        "\nyour cost (number of actions):", action_count,
        '\n\ncurrent map state:'
    )
    gui.display(game)
